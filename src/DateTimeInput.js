
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import { addField, FieldTitle } from 'ra-core';

// import sanitizeRestProps from 'react-admin/sanitizeRestProps';

/**
 * Convert Date object to String
 * 
 * @param {Date} v value to convert
 * @returns {String} A standardized date (yyyy-MM-dd), to be passed to an <input type="date" />
 */
const dateFormatter = v => {
    if (!(v instanceof Date) || isNaN(v)) return;
    // return v.toISOString().slice(0, 16);
    const pad = '00';
    const yyyy = v.getFullYear().toString();
    const MM = (v.getMonth() + 1).toString();
    const dd = v.getDate().toString();
    const hh = v.getHours().toString();
    const mm = v.getMinutes().toString();
    return `${yyyy}-${(pad + MM).slice(-2)}-${(pad + dd).slice(-2)}T${(pad + hh).slice(-2)}:${(pad + mm).slice(-2)}`;
};

const sanitizeValue = value => {
    // null, undefined and empty string values should not go through dateFormatter
    // otherwise, it returns undefined and will make the input an uncontrolled one.    
    if (value == null || value === '') {
        return '';
    }
    const finalValue = typeof value instanceof Date ? value : new Date(value);    
    return dateFormatter(finalValue);
};

export class DateTimeInput extends Component {
    onChange = event => {
        this.props.input.onChange(event.target.value);
    };

    render() {
        const {
            className,
            meta,
            input,
            isRequired,
            label,
            options,
            source,
            resource,
            ...rest
        } = this.props;
        if (typeof meta === 'undefined') {
            throw new Error(
                "The DateInput component wasn't called within a redux-form <Field>. Did you decorate it and forget to add the addField prop to your component? See https://marmelab.com/react-admin/Inputs.html#writing-your-own-input-component for details."
            );
        }
        const { touched, error } = meta;
        const value = sanitizeValue(input.value);
        
        return (
            <TextField
                {...input}
                className={className}
                type="datetime-local"
                margin="normal"
                error={!!(touched && error)}
                helperText={touched && error}
                label={
                    <FieldTitle
                        label={label}
                        source={source}
                        resource={resource}
                        isRequired={isRequired}
                    />
                }
                InputLabelProps={{
                    shrink: true,
                }}
                {...options}
                {...sanitizeRestProps(rest)}
                value={value}
                onChange={this.onChange}
                onBlur={this.onBlur}
            />
        );
    }
}

DateTimeInput.propTypes = {
    classes: PropTypes.object,
    className: PropTypes.string,
    input: PropTypes.object,
    isRequired: PropTypes.bool,
    label: PropTypes.string,
    meta: PropTypes.object,
    options: PropTypes.object,
    resource: PropTypes.string,
    source: PropTypes.string,
};

DateTimeInput.defaultProps = {
    options: {},
};

export default addField(DateTimeInput);

// import React from 'react';
// import TimePicker from 'material-ui-time-picker';

// // import { Labeled } from 'react-admin';
// import { Field } from 'redux-form';

// const DateTimeInput = (props) => {
//     console.log(props);
    
//     let label = props.label ? props.label : props.source;

//     let value = props.record.time_start ? props.record.time_start : new Date();

//     return (
//     <span>
//         <span>{label}</span>
//         <br/>
//         <Field name={props.source} component="input" type="datetime-local" defaultValue={value}/>
//     </span>
// )};

// export default DateTimeInput;

const sanitizeRestProps = ({
    alwaysOn,
    basePath,
    component,
    defaultValue,
    formClassName,
    initializeForm,
    input,
    isRequired,
    label,
    locale,
    meta,
    options,
    optionText,
    optionValue,
    record,
    resource,
    allowEmpty,
    source,
    textAlign,
    translate,
    translateChoice,
    ...rest
}) => rest;