import React from 'react';
import { List, Edit, Create, Datagrid, TextField, DateField, EditButton, DisabledInput, TextInput, LongTextInput, SimpleForm} from 'react-admin';

export const SubscriberList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="user-agent" />
            <TextField source="ip" />
            <EditButton />
        </Datagrid>
    </List>
);

const SubscriberTitle = ({ record }) => {
    return <span>Subscriber {record ? `"${record.id}"` : ''}</span>;
};

export const SubscriberEdit = (props) => (
    <Edit title={<SubscriberTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <DisabledInput source="user-agent" />
            <DisabledInput source="ip" />
            <DisabledInput source="subscription.endpoint" />
        </SimpleForm>
    </Edit>
);