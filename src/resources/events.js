import React from 'react';
import { List, Edit, Create, Datagrid, TextField, DateField, EditButton, DisabledInput, TextInput, LongTextInput, BooleanInput, SimpleForm} from 'react-admin';
import DateTimeInput from '../DateTimeInput';

export const EventList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="title" />
            <TextField source="description" />
            <DateField source="time-start" showTime />
            <DateField source="time-end" showTime />
            <EditButton />
        </Datagrid>
    </List>
);

const EventTitle = ({ record }) => {
    return <span>Event {record ? `"${record.title}"` : ''}</span>;
};

const buttonStyle = {
    margin: 12,
};

export const EventEdit = (props) => (
    <Edit title={<EventTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="title" />
            <LongTextInput source="description" />
            <DateTimeInput source="time-start" label="Start Time"/>
            <DateTimeInput source="time-end" label="End Time"/>
            <BooleanInput source="notify" label="Notify" />
        </SimpleForm>
    </Edit>
);


export const EventCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="title" />
            <LongTextInput source="description" />

            <DateTimeInput source="time-start" label="Start Time"/>
            <DateTimeInput source="time-end" label="End Time"/>
        </SimpleForm>
    </Create>
);