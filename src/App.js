import React from 'react';
import { Admin, Resource } from 'react-admin';
import jsonAPIRestClient from './dataprovider/restClient'
import { EventList, EventCreate, EventEdit } from './resources/events'
import { SubscriberList, SubscriberEdit } from './resources/subscribers'
import authProvider from './authProvider'

const endpoint = (process.env.NODE_ENV === 'production') ? 'https://adam-notifications-prod.herokuapp.com/api/' : `http://${window.location.hostname}:5555/api/`

const dataProvider = jsonAPIRestClient(endpoint);
// const dataProvider = jsonServerProvider('http://jsonplaceholder.typicode.com');
// console.log(window.location.hostname);


const App = () => (
  <Admin dataProvider={dataProvider} authProvider={authProvider}>
    <Resource name="events" list={EventList} edit={EventEdit} create={EventCreate}/>
    <Resource name="subscribers" list={SubscriberList} edit={SubscriberEdit}/>
  </Admin>
);

export default App;